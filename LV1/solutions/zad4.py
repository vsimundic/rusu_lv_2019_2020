# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 13:17:44 2019

@author: Valentin
"""

"""
Napišite program koji od korisnika zahtijeva unos brojeva u beskonačnoj petlji 
sve dok korisnik ne upiše „Done“ (bez navodnika). Nakon toga potrebno je 
ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i 
maksimalnu vrijednost. Osigurajte program od krivog unosa (npr. slovo umjesto 
brojke) na način da program zanemari taj unos i ispiše odgovarajuću poruku
"""

list_numbers = []

while(True):
    x = input("Enter a value: ")
    if x == "Done":
        break
    try:
        list_numbers.append(float(x))
    except ValueError:
        print("Not a number.")
        
print("List size: ", len(list_numbers))
print("List min: ", min(list_numbers))
print("List max: ", max(list_numbers))
print("List average: ", sum(list_numbers)/len(list_numbers))