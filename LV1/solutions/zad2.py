# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 12:46:05 2019

@author: Valentin
"""
import sys

grade = input ("Enter a grade: ")

try:
    grade = float(grade)
except ValueError:
    print("You didn't enter a number!")
    sys.exit()

print(type(grade))
grade_str=""
if grade >= 0.0 and grade <= 1.0:
    if grade < 0.6:
        grade_str = "F"
    elif grade >= 0.6 and grade < 0.7:
        grade_str = "D"
    elif grade >= 0.7 and grade < 0.8:
        grade_str = "C"
    elif grade >= 0.8 and grade < 0.9:
        grade_str = "B"
    else:
        grade_str = "A"
else:
    print("Number not in range from 0.0 to 1.0")
    sys.exit()

print("Grade: " + grade_str)
