# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 13:52:55 2019

@author: Valentin
"""

"""Napišite Python skriptu koja će učitati tekstualnu datoteku te iz redova koji počinju s „From“ izdvojiti mail adresu te ju
spremiti u listu. Nadalje potrebno je napraviti dictionary koji sadrži hostname (dio emaila iza znaka @) te koliko puta se
pojavljuje svaki hostname u učitanoj datoteci. Koristite datoteku mbox-short.txt. Na ekran ispišite samo nekoliko
email adresa te nekoliko zapisa u dictionary-u."""

import re

filename = input("Enter filename: ")
file = open(filename)

lst = []

for line in file:
    if line.startswith("From"):
        match = re.search(r'[\w\.-]+@[\w\.-]+', line)
        lst.append(match.group(0))

dic = dict()
for k in lst:
    domain = k.split('@')[1]
    if domain not in dic:
        dic[domain] = 1
    else:
        dic[domain] +=1

print(dic)

