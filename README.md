# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2019./2020.


## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).


## Podaci o studentu:

Ime i prezime: Valentin Šimundić

## Ispravljene pogreške
line 3: ispravljeno raw_input u input
line 5: ispravljeno fnamex u fname
line 7: ispravljene zagrade
line 18: dodano += umjesto =
line 20: ispravljene zagrade