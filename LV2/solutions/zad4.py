# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 12:27:38 2019

@author: Valentin
"""

import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0) # postavi seed generatora brojeva
die = np.random.randint(low=1, high=7, size=100)
print(die)
plt.hist(die, bins=20)